/*
 * project: circular buffer, cbuffer
 * brief: simple, bare-minimal implementation for circular buffer
 * tags: circular buffer, fifo, 
 * Copyright (c) 2017 - 2025.
 * All Rights Reserved.
 * 
 * creator: Hardik Madaan
 * alter-ego: Brian doofus 
 * mail: hardik.mad@gmail.com
 * web: www.tachymoron.wordpress.com
 * git: https://bitbucket.org/pundoobvi/
 *
 */
#include "cbuffer.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"

bool is_cbuffer_empty(cbuffer_s * cbuffer)
{
	bool state = FALSE;

	if(NULL == cbuffer)
	{
		return (1);	
	}

	state = cbuffer->read == cbuffer->write;
	return (state);
}

bool is_cbuffer_full(cbuffer_s * cbuffer)
{
	bool state = FALSE;

	if(NULL == cbuffer)
	{
		return (1);	
	}

	state = cbuffer->read == ( (cbuffer->write + 1) % cbuffer->size);
	return (state);
}


rt cbuffer_init(cbuffer_s ** cbuffer_dp, uint32_t size)
{
	rt status = RT_SUCCESS;

	if(NULL == (cbuffer_dp))	
	{
		status = RT_NULL_PTR;
		return (status);
	}

	if(RT_SUCCESS == status)
	{
		*(cbuffer_dp) = (cbuffer_s *) malloc( sizeof(cbuffer_s) );
		// if memory allocation fails
		if(NULL == (*cbuffer_dp))
		{
			status = RT_MALLOC_FAILED;
			return (status);		
		}	
		else
		{
			(*cbuffer_dp)->buffer = (cbuf_data_t *) malloc(size  * sizeof(cbuf_data_t) );
			// if memory allocation fails
			if(NULL == (*cbuffer_dp)->buffer)	
			{
				status = RT_MALLOC_FAILED;
				return (status);
			}
			else
			{
				memset( (void *) (*cbuffer_dp)->buffer, 0x0, size  * sizeof(cbuf_data_t) );
			}
		}
	}
	
	if(RT_SUCCESS == status)
	{
		status = cbuffer_set_to_default(*cbuffer_dp, size, FALSE);
  }

  return (status);
}

rt cbuffer_write(cbuffer_s *cbuffer , cbuf_data_t data)
{
	rt status = RT_SUCCESS;
	
	if(NULL == cbuffer)
	{
		status = RT_NULL_PTR;
		return (status);	
	}

	if(RT_SUCCESS == status)
	{
		cbuffer->buffer[cbuffer->write] = data;
		cbuffer->write = (cbuffer->write + 1U) % cbuffer->size;
		if(cbuffer->write == cbuffer->read)
		{
			cbuffer->read = (cbuffer->read + 1U) % cbuffer->size;
		}
	}
	return (status);
}

rt cbuffer_read(cbuffer_s *cbuffer , cbuf_data_t * data_p)
{
	rt status = RT_SUCCESS;

	if(NULL == cbuffer)
	{
		status = RT_NULL_PTR;
		return (status);	
	}
	if(TRUE == is_cbuffer_empty(cbuffer) )
	{
		return 0;
	}
	if(NULL ==  data_p)
	{
		status = RT_NULL_PTR;
		return (status);
	}

	if(RT_SUCCESS == status)
	{
		*data_p = cbuffer->buffer[cbuffer->read];
		cbuffer->read = (cbuffer->read + 1U) % cbuffer->size;
	}
	return (status);
}

rt cbuffer_set_to_default(cbuffer_s * cbuffer, uint32_t size, uint8_t resetf)
{
	rt status = RT_SUCCESS;

	if(NULL == cbuffer)
	{
		status = RT_NULL_PTR;		
		return (status);	
	}

	if(RT_SUCCESS == status)
	{
		if(TRUE == resetf)
		{
			free(cbuffer->buffer);
		}
		else
		{
			// do nothing
		}

		cbuffer->size = size; 
		cbuffer->write = 0;
		cbuffer->read = 0;
		cbuffer->full = 0;
	}
	return (status);
}

#ifdef _PRINTF_
	rt cbuffer_print(cbuffer_s *cbuffer)
	{
		rt status = RT_SUCCESS;
		uint32_t counter = 0U;

		if(NULL == cbuffer)
		{
			status = RT_NULL_PTR;
			return (status);
		}

		if(RT_SUCCESS == status)
		{
			printf("\ncBuffer:");
			printf("\n\tSize @ %u Read @ %u Write @ %u \t", cbuffer->size, cbuffer->read, cbuffer->write);
			if(cbuffer->full)
			{
				printf("Full");
			}
			else
			{
				printf("NotFull");
			}

			printf("\ncBuffer's content\n\t");
			for(counter = 0;  counter < cbuffer->size ; counter++)
			{
				printf("%u \t", cbuffer->buffer[counter]);
			}
		}

		return(status);
	}
#endif /* _PRINTF_ */
