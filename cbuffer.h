/*
 * project: circular buffer, cbuffer
 * brief: simple, bare-minimal implementation for circular buffer
 * tags: circular buffer, fifo, 
 * Copyright (c) 2017 - 2025.
 * All Rights Reserved.
 * 
 * creator: Hardik Madaan
 * alter-ego: Brian doofus 
 * mail: hardik.mad@gmail.com
 * web: www.tachymoron.wordpress.com
 * git: https://bitbucket.org/pundoobvi/
 *
 */

/*
 * project: breadbutter, generic
 * brief: plug and play, simple application modules.
 * tags: standard return types, macros, 
 * Copyright (c) 2017 - 2025.
 * All Rights Reserved.
 * 
 * creator: Hardik Madaan
 * alter-ego: Brian doofus 
 * mail: hardik.mad@gmail.com
 * web: www.tachymoron.wordpress.com
 * git: https://bitbucket.org/bacon_toast/
 *
 */
#ifndef _CBUFFER_H_
#define _CBUFFER_H_
/*=============================================================================
															 		File-Includes
=============================================================================*/
#include "../cstm_std/cstm_std_types.h"
#include "stdint.h"
#include "stdbool.h"

/*=============================================================================
														  		MACROs-&-ENUMs
=============================================================================*/
/*-------Config Macros--------*/
#define _DEBUG_EN_
#define _PRINTF_

/*=============================================================================
							 								 	Type-Definitions
=============================================================================*/
/* typedefintion for datatype of data to be stored in the buffer */
typedef uint16_t cbuf_data_t;

/* typedef strcuture for circular buffer */
typedef struct
{
	/* cbuffer pointer, void type by default */
	cbuf_data_t * buffer;
	/* cbuffer Size */
	uint32_t size;
	/* write and read pointers */
	uint32_t write, read;
	/* Buffer full flag */
	bool full;

}cbuffer_s;

/*=============================================================================
							 							   Extern-Declarations
=============================================================================*/

/*=============================================================================
							 						    Function-Declarations
=============================================================================*/
/* is circular buffer empty */
bool is_cbuffer_empty(cbuffer_s * cbuffer);

/* is circular buffer full */
bool is_cbuffer_full(cbuffer_s * cbuffer);

/* inits circular buffer */
rt cbuffer_init(cbuffer_s ** cbuffer, uint32_t size);

/* write data into buffer */
rt cbuffer_write(cbuffer_s * cbuffer , cbuf_data_t data);

/* read data from buffer */
rt cbuffer_read(cbuffer_s * cbuffer, cbuf_data_t * data_p);

/* circular buffer set to default */
rt cbuffer_set_to_default(cbuffer_s * cbuffer, uint32_t size, uint8_t resetf);

/* write bulk data into buffer */
rt cbuffer_write_l(cbuffer_s * cbuffer, cbuf_data_t * data_p, uint32_t size);

/* cbuffer demo/test function */
rt cbuffer_demo_main();
/* DEBUG Functions */
#ifdef _PRINTF_
	/* print cBuffer */
	rt cbuffer_print(cbuffer_s *cbuffer);
#endif /* _PRINTF_ */

#endif /* _CBUFFER_H_ */