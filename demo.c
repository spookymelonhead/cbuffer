#include "stdio.h"
#include "stdbool.h"
#include "../cstm_std/cstm_std_types.h"
#include "cbuffer.h"

rt cbuffer_demo_main()
{
	rt status = RT_SUCCESS;
	cbuffer_s * cbuffer = NULL;
	uint16_t data = 0U;

	CHECK_STATUS(status);
	status = cbuffer_init(&cbuffer, 10);
	CHECK_STATUS(status);
	status = cbuffer_print(cbuffer);
	CHECK_STATUS(status);
	status = cbuffer_write(cbuffer, 23U);
	CHECK_STATUS(status);
	status = cbuffer_write(cbuffer, 24U);
	CHECK_STATUS(status);
	status = cbuffer_write(cbuffer, 25U);
	CHECK_STATUS(status);
	status = cbuffer_write(cbuffer, 26U);
	CHECK_STATUS(status);
	status = cbuffer_write(cbuffer, 27U);
	CHECK_STATUS(status);
	status = cbuffer_write(cbuffer, 69U);
	CHECK_STATUS(status);
	status = cbuffer_print(cbuffer);
	CHECK_STATUS(status);
	status = cbuffer_read(cbuffer, &data);
	CHECK_STATUS(status);
	printf("\n%u", data);
	status = cbuffer_read(cbuffer, &data);
	CHECK_STATUS(status);
	printf("\n%u", data);
	status = cbuffer_print(cbuffer);
	CHECK_STATUS(status);
	
	printf("\n");
	return (status);
}